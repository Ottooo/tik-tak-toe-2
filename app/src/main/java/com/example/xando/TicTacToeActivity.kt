package com.example.xando

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.tic_tac_toe_main_activity.*

class TicTacToeActivity : AppCompatActivity() {
    private var firstPlayer = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tic_tac_toe_main_activity)
        init()
    }

    private fun init() {
        Button11.setOnClickListener() {
            GuessAndChangeButton(Button11)

        }
        Button12.setOnClickListener() {
            GuessAndChangeButton(Button12)

        }
        Button13.setOnClickListener() {
            GuessAndChangeButton(Button13)

        }
        Button21.setOnClickListener() {
            GuessAndChangeButton(Button21)

        }
        Button22.setOnClickListener() {
            GuessAndChangeButton(Button22)

        }
        Button23.setOnClickListener() {
            GuessAndChangeButton(Button23)

        }
        Button31.setOnClickListener() {
            GuessAndChangeButton(Button31)

        }
        Button32.setOnClickListener() {
            GuessAndChangeButton(Button32)

        }
        Button33.setOnClickListener() {
            GuessAndChangeButton(Button33)

        }
        ResetButton.setOnClickListener() {
            firstPlayer = true
            Button11.isClickable = true
            Button11.text = ""
            Button12.isClickable = true
            Button12.text = ""
            Button13.isClickable = true
            Button13.text = ""
            Button21.isClickable = true
            Button21.text = ""
            Button22.isClickable = true
            Button22.text = ""
            Button23.isClickable = true
            Button23.text = ""
            Button31.isClickable = true
            Button31.text = ""
            Button32.isClickable = true
            Button32.text = ""
            Button33.isClickable = true
            Button33.text = ""


        }

    }

    private fun GuessAndChangeButton(Button: Button) {
        d("onClick", "Button")
        if (firstPlayer) {
            Button.text = "X"
        } else {
            Button.text = "O"
        }
        Button.isClickable = false
        firstPlayer = !firstPlayer
        checkWinner()

    }

    private fun checkWinner() {
        if (Button11.text.isNotEmpty() && Button11.text.toString() == Button12.text.toString() && Button11.text.toString() == Button13.text.toString()) {
            Toast.makeText(this, "Winner is ${Button11.text.toString()}", Toast.LENGTH_LONG).show()
            clickableFalse()
        } else if (Button21.text.isNotEmpty() && Button21.text.toString() == Button22.text.toString() && Button21.text.toString() == Button23.text.toString()) {
            Toast.makeText(this, "Winner is ${Button21.text.toString()}", Toast.LENGTH_LONG).show()
            clickableFalse()
        } else if (Button31.text.isNotEmpty() && Button31.text.toString() == Button32.text.toString() && Button31.text.toString() == Button33.text.toString()) {
            Toast.makeText(this, "Winner is ${Button31.text.toString()}", Toast.LENGTH_LONG).show()
            clickableFalse()
        } else if (Button11.text.isNotEmpty() && Button11.text.toString() == Button21.text.toString() && Button11.text.toString() == Button31.text.toString()) {
            Toast.makeText(this, "Winner is ${Button11.text.toString()}", Toast.LENGTH_LONG).show()
            clickableFalse()
        } else if (Button12.text.isNotEmpty() && Button12.text.toString() == Button22.text.toString() && Button12.text.toString() == Button32.text.toString()) {
            Toast.makeText(this, "Winner is ${Button22.text.toString()}", Toast.LENGTH_LONG).show()
            clickableFalse()
        } else if (Button13.text.isNotEmpty() && Button13.text.toString() == Button23.text.toString() && Button13.text.toString() == Button33.text.toString()) {
            Toast.makeText(this, "Winner is ${Button13.text.toString()}", Toast.LENGTH_LONG).show()
            clickableFalse()
        } else if (Button11.text.isNotEmpty() && Button11.text.toString() == Button22.text.toString() && Button11.text.toString() == Button33.text.toString()) {
            Toast.makeText(this, "Winner is ${Button11.text.toString()}", Toast.LENGTH_LONG).show()
            clickableFalse()
        } else if (Button13.text.isNotEmpty() && Button13.text.toString() == Button22.text.toString() && Button13.text.toString() == Button31.text.toString()) {
            Toast.makeText(this, "Winner is ${Button31.text.toString()}", Toast.LENGTH_LONG).show()
            clickableFalse()
        } else if (Button11.text.isNotEmpty() && Button13.text.isNotEmpty() && Button12.text.isNotEmpty() && Button21.text.isNotEmpty() && Button22.text.isNotEmpty() && Button23.text.isNotEmpty() && Button31.text.isNotEmpty() && Button32.text.isNotEmpty() && Button33.text.isNotEmpty()) {
            Toast.makeText(this, "No one is winner", Toast.LENGTH_LONG).show()
            clickableFalse()
        }

    }
    private fun clickableFalse() {
        Button11.isClickable = false
        Button12.isClickable = false
        Button13.isClickable = false
        Button21.isClickable = false
        Button22.isClickable = false
        Button23.isClickable = false
        Button31.isClickable = false
        Button32.isClickable = false
        Button33.isClickable = false
    }


    }



